﻿using JustBlog.Entity.Entity;
using JustBlog.Model.Response;
using JustBlog.Model.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.Repository.AccountRepository
{
    public interface IAccountRepository
    {
        Task<string> EncodeSha256(User user, string role);

        Task<Response<string>> Authencate(LoginModel model);
    }
}