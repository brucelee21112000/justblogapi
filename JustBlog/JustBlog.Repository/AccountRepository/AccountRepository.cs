﻿using JustBlog.Common;
using JustBlog.Entity.Context;
using JustBlog.Entity.Entity;
using JustBlog.Model.Response;
using JustBlog.Model.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.Repository.AccountRepository
{
    public class AccountRepository : Repository<AccountRepository>, IAccountRepository
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly IConfiguration _configuration;

        public AccountRepository(JustBlogDbContext context,
                                 ILogger<AccountRepository> logger,
                                 UserManager<User> userManager,
                                 SignInManager<User> signInManager,
                                 IConfiguration configuration,
                                 RoleManager<Role> roleManager) : base(context, logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _roleManager = roleManager;
        }

        public async Task<Response<string>> Authencate(LoginModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                var user = _context.Users.Where(x => x.UserName == model.UserName && x.IsDelete == false).FirstOrDefault();
                if (user == null)
                {
                    res.Code = StatusCodes.Status404NotFound;
                    res.Message = "Tài khoản hoặc mật khẩu bị sai!";
                    return res;
                }
                else
                {
                    var checkPassword = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);
                    if (checkPassword.Succeeded)
                    {
                        var role = await _userManager.GetRolesAsync(user);
                        var newToken = await EncodeSha256(user, string.Join(", ", role));
                        res.Data = newToken;
                        return res;
                    }
                    else
                    {
                        res.Code = StatusCodes.Status404NotFound;
                        res.Message = "Tài khoản hoặc mật khẩu bị sai!";
                        return res;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
            }
            return res;
        }

        public async Task<string> EncodeSha256(User user, string role)
        {
            string token = null;
            var key = _configuration["Tokens:Key"];
            var issuer = _configuration["Tokens:Issuer"];
            var expires = _configuration["Tokens:Expires"];

            if (string.IsNullOrEmpty(key))
            {
                key = "FAManagementFAManagementFAManagement";
            }

            if (string.IsNullOrEmpty(issuer))
            {
                issuer = "FAManagement";
            }

            if (string.IsNullOrEmpty(expires))
            {
                expires = "1*24*60*60";
            }

            var arr = expires.Split('*');
            double seconds = 1;
            try
            {
                foreach (var item in arr)
                {
                    seconds *= Int32.Parse(item.ToString());
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                seconds = 1 * 24 * 60 * 60;
            }

            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            claims.Add(new Claim(ClaimTypes.Role, role == null ? "" : role));
            claims.Add(new Claim(ClaimTypes.Name, user.FullName));

            token = TokenUtil.EncodeSha256(claims, key, issuer, seconds);

            return await Task.FromResult(token);
        }
    }
}