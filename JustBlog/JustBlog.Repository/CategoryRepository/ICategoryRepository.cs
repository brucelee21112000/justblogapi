﻿using JustBlog.Model.Category;
using JustBlog.Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.Repository.CategoryRepository
{
    public interface ICategoryRepository
    {
        Task<Response<CategoryViewModel>> GetAllCategory();

        Task<Response<string>> CreateCategory(CategoryCreateModel model);

        Task<Response<CategoryViewModel>> GetCategory(Guid categoryId);

        Task<Response<string>> UpdateCategory(CategoryUpdateModel model);

        Task<Response<string>> DeleteCategory(Guid categoryId);
    }
}