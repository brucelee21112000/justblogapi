﻿using JustBlog.Common;
using JustBlog.Entity.Context;
using JustBlog.Entity.Entity;
using JustBlog.Model.Category;
using JustBlog.Model.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.Repository.CategoryRepository
{
    public class CategoryRepository : Repository<CategoryRepository>, ICategoryRepository
    {
        public CategoryRepository(JustBlogDbContext context, ILogger<CategoryRepository> logger) : base(context, logger)
        {
        }

        public async Task<Response<CategoryViewModel>> GetAllCategory()
        {
            Response<CategoryViewModel> res = new Response<CategoryViewModel>();
            try
            {
                var cates = await _context.Categories.Where(x => x.IsDelete == false).ToListAsync();
                if (cates.Count > 0)
                {
                    var result = new List<CategoryViewModel>();
                    foreach (var item in cates)
                    {
                        result.Add(new CategoryViewModel()
                        {
                            Id = item.Id,
                            CategoryName = item.Name,
                            UrlSlug = item.UrlSlug,
                            Description = item.Description,
                        });
                    }

                    res.Message = "Get category list successfully!";
                    res.Code = StatusCodes.Status200OK;
                    res.DataList = result;
                }
                else
                {
                    res.Message = "Get category list failed!";
                    res.Code = StatusCodes.Status404NotFound;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Message = "Failed!";
                res.Code = StatusCodes.Status500InternalServerError;
            }
            return res;
        }

        public async Task<Response<string>> CreateCategory(CategoryCreateModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                if (model.CategoryName == null)
                {
                    res.Message = "Category name can't be empty!";
                    res.Code = StatusCodes.Status404NotFound;
                    return res;
                }
                if (model.Description == null)
                {
                    res.Message = "Description can't be empty!";
                    res.Code = StatusCodes.Status404NotFound;
                    return res;
                }

                var cate = new Category();
                cate.Id = Guid.NewGuid();
                cate.Name = model.CategoryName;
                cate.Description = model.Description;
                cate.UrlSlug = SeoUrlHepler.FrientlyUrl(model.CategoryName);
                _context.Categories.Add(cate);

                var count = await _context.SaveChangesAsync();
                if (count > 0)
                {
                    res.Message = "Create category successfully!";
                    res.Code = StatusCodes.Status200OK;
                }
                else
                {
                    res.Message = "Create category failed!";
                    res.Code = StatusCodes.Status400BadRequest;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Message = "Failed!";
                res.Code = StatusCodes.Status500InternalServerError;
            }
            return res;
        }

        public async Task<Response<CategoryViewModel>> GetCategory(Guid categoryId)
        {
            Response<CategoryViewModel> res = new Response<CategoryViewModel>();
            try
            {
                var cate = await _context.Categories.FindAsync(categoryId);
                if (cate != null && cate.IsDelete == false)
                {
                    var result = new CategoryViewModel()
                    {
                        Id = cate.Id,
                        CategoryName = cate.Name,
                        Description = cate.Description,
                        UrlSlug = cate.UrlSlug
                    };

                    res.Data = result;
                    res.Message = "Get category successfully!";
                    res.Code = StatusCodes.Status200OK;
                }
                else
                {
                    res.Message = "Not found category!";
                    res.Code = StatusCodes.Status404NotFound;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Message = "Failed!";
                res.Code = StatusCodes.Status500InternalServerError;
            }
            return res;
        }

        public async Task<Response<string>> UpdateCategory(CategoryUpdateModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                if (model.CategoryName == null)
                {
                    res.Message = "Category name can't be empty!";
                    res.Code = StatusCodes.Status404NotFound;
                    return res;
                }
                if (model.Description == null)
                {
                    res.Message = "Description can't be empty!";
                    res.Code = StatusCodes.Status404NotFound;
                    return res;
                }
                var cate = await _context.Categories.FindAsync(model.Id);
                if (cate != null)
                {
                    cate.Name = model.CategoryName;
                    cate.Description = model.Description;
                    cate.UrlSlug = SeoUrlHepler.FrientlyUrl(model.CategoryName);
                    _context.Categories.Update(cate);
                    var count = await _context.SaveChangesAsync();
                    if (count > 0)
                    {
                        res.Message = "Update category successfully!";
                        res.Code = StatusCodes.Status200OK;
                    }
                    else
                    {
                        res.Message = "Update category failed!";
                        res.Code = StatusCodes.Status400BadRequest;
                    }
                }
                else
                {
                    res.Message = "Not found category!";
                    res.Code = StatusCodes.Status404NotFound;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Message = "Failed!";
                res.Code = StatusCodes.Status500InternalServerError;
            }
            return res;
        }

        public async Task<Response<string>> DeleteCategory(Guid categoryId)
        {
            Response<string> res = new Response<string>();
            try
            {
                var cate = await _context.Categories.FindAsync(categoryId);
                if (cate != null)
                {
                    cate.IsDelete = true;
                    _context.Categories.Update(cate);
                    var count = await _context.SaveChangesAsync();
                    if (count > 0)
                    {
                        res.Message = "Delete category successfully!";
                        res.Code = StatusCodes.Status200OK;
                    }
                    else
                    {
                        res.Message = "Delete category failed!";
                        res.Code = StatusCodes.Status400BadRequest;
                    }
                }
                else
                {
                    res.Message = "Not found category!";
                    res.Code = StatusCodes.Status404NotFound;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Message = "Failed!";
                res.Code = StatusCodes.Status500InternalServerError;
            }
            return res;
        }
    }
}