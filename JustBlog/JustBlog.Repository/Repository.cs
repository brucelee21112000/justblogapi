﻿using JustBlog.Entity.Context;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.Repository
{
    public abstract class Repository<T>
    {
        public readonly JustBlogDbContext _context;
        public readonly ILogger<T> _logger;

        public Repository(JustBlogDbContext context, ILogger<T> logger)
        {
            _context = context;
            _logger = logger;
        }
    }
}