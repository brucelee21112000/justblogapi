﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.Model.Category
{
    public class CategoryCreateModel
    {
        public string CategoryName { get; set; }
        public string Description { get; set; }
    }
}