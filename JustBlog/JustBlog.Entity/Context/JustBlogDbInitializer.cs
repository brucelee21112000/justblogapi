﻿using JustBlog.Entity.Entity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.Entity.Context
{
    public static class JustBlogDbInitializer
    {
        public static void Seed(this ModelBuilder builder)
        {
            builder.Entity<Role>().HasData(
                new Role()
                {
                    Id = Guid.Parse("46118fe1-2d15-4c5b-82f9-bc2f19b4a7c3"),
                    Name = "Admin",
                    NormalizedName = "Admin".ToUpper()
                }
            );

            var hasher = new PasswordHasher<User>();
            builder.Entity<User>().HasData(
                new User()
                {
                    Id = Guid.Parse("e3b5d306-49b2-46b2-a345-fc9d644f6700"),
                    UserName = "admin",
                    FullName = "Admin",
                    NormalizedUserName = "ADMIN",
                    Email = "admin@gmail.com",
                    IsActive = true,
                    IsDelete = false,
                    SecurityStamp = Guid.NewGuid().ToString("D"),
                    PasswordHash = hasher.HashPassword(null, "Admin@123")
                }
            );

            builder.Entity<IdentityUserRole<Guid>>().HasData(
                new IdentityUserRole<Guid>
                {
                    RoleId = Guid.Parse("46118fe1-2d15-4c5b-82f9-bc2f19b4a7c3"),
                    UserId = Guid.Parse("e3b5d306-49b2-46b2-a345-fc9d644f6700")
                }
            );
        }
    }
}