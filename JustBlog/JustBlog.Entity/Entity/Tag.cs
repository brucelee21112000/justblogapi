﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.Entity.Entity
{
    public class Tag : EntityBase
    {
        public string Name { get; set; }
        public string UrlSlug { get; set; }

        public string Decription { get; set; }
        public int Count { get; set; }
        public IList<PostTagMap> PostTagMaps { get; set; }
    }
}