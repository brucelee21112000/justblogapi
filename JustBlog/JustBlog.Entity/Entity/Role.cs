﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustBlog.Entity.Entity
{
    public class Role : IdentityRole<Guid>
    {
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
    }
}