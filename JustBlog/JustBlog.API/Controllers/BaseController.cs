﻿using Microsoft.AspNetCore.Mvc;

namespace JustBlog.API.Controllers
{
    public class BaseController : Controller
    {
        public readonly ILogger<BaseController> _logger;

        public BaseController(ILogger<BaseController> logger)
        {
            _logger = logger;
        }
    }
}