﻿using JustBlog.Entity.Entity;
using JustBlog.Model.Response;
using JustBlog.Model.User;
using JustBlog.Repository.AccountRepository;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace JustBlog.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : BaseController
    {
        private readonly IAccountRepository _accountRepository;
        private readonly UserManager<User> _userManager;

        public AccountController(
            UserManager<User> userManager,
            ILogger<BaseController> logger,
            IAccountRepository accountRepository) : base(logger)
        {
            _accountRepository = accountRepository;
            _userManager = userManager;
        }

        [Route("Authencate")]
        [HttpPost]
        public async Task<Response<string>> Authencate(LoginModel model)
        {

            return await _accountRepository.Authencate(model);
        }
    }
}