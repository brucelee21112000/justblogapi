﻿using JustBlog.Model.Category;
using JustBlog.Model.Response;
using JustBlog.Repository.CategoryRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JustBlog.API.Controllers
{
    [Route("api/categories")]
    [ApiController]
    public class CategoryController : BaseController
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryController(ICategoryRepository categoryRepository, ILogger<BaseController> logger) : base(logger)
        {
            _categoryRepository = categoryRepository;
        }

        [HttpGet]
        public async Task<Response<CategoryViewModel>> GetAllCategory()
        {
            return await _categoryRepository.GetAllCategory();
        }

        [HttpPost]
        public async Task<Response<string>> CreateCategory(CategoryCreateModel model)
        {
            return await _categoryRepository.CreateCategory(model);
        }

        [HttpGet("{id}")]
        public async Task<Response<CategoryViewModel>> GetCategory(Guid id)
        {
            return await _categoryRepository.GetCategory(id);
        }

        [HttpPut("{id}")]
        public async Task<Response<string>> UpdateCategory(CategoryUpdateModel model)
        {
            return await _categoryRepository.UpdateCategory(model);
        }

        [HttpDelete("{id}")]
        public async Task<Response<string>> DeleteCategory(Guid id)
        {
            return await _categoryRepository.DeleteCategory(id);
        }
    }
}